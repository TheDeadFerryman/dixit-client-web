const {addAfterLoader, removeLoaders, loaderByName, getLoaders, throwUnexpectedConfigError} = require('@craco/craco');

module.exports = {
	eslint: {
		mode: 'file',
		pluginOptions: ({rulePaths, ...options}) => ({
			...options,
			rulePaths: (rulePaths ?? []).concat([
				'./.eslint'
			])
		})
	},
	webpack: {
		configure: (webpackConfig, { paths }) => {
			const { hasFoundAny, matches } = getLoaders(webpackConfig, loaderByName('babel-loader'));
			if (!hasFoundAny) throwUnexpectedConfigError('failed to find babel-loader');

			console.log('removing babel-loader');
			const { hasRemovedAny, removedCount } = removeLoaders(webpackConfig, loaderByName('babel-loader'));
			if (!hasRemovedAny) throwUnexpectedConfigError('no babel-loader to remove');
			if (removedCount !== 2) throwUnexpectedConfigError('had expected to remove 2 babel loader instances');

			console.log('adding ts-loader');

			const tsLoader = {
				test: /\.(js|mjs|jsx|ts|tsx)$/,
				include: paths.appSrc,
				loader: require.resolve('ts-loader'),
				options: { transpileOnly: true },
			};

			const { isAdded: tsLoaderIsAdded } = addAfterLoader(webpackConfig, loaderByName('url-loader'), tsLoader);
			if (!tsLoaderIsAdded) throwUnexpectedConfigError('failed to add ts-loader');
			console.log('added ts-loader');

			console.log('adding non-application JS babel-loader back');
			const { isAdded: babelLoaderIsAdded } = addAfterLoader(
				webpackConfig,
				loaderByName('ts-loader'),
				matches[1].loader, // babel-loader
			);
			if (!babelLoaderIsAdded) throwUnexpectedConfigError('failed to add back babel-loader for non-application JS');
			console.log('added non-application JS babel-loader back');

			webpackConfig.output.libraryTarget = 'window';

			return webpackConfig;
		},
	},
};