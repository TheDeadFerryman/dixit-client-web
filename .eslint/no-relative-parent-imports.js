const {join, dirname, relative} = require('path');

module.exports = {
  meta: {
    type: 'problem',
    docs: {},
    fixable: 'code',
    schema: [
      {type: 'string'},
    ],
    messages: {
      noRelativeUntilNecessary: 'Avoid using relative imports from parent directories. You can use \'{{ newPath }}\' here instead.',
    },
  },
  create: context => ({
    'ImportDeclaration Literal': node => {
      const {value: importPath, raw} = node;

      const [root] = context.options;

      if (!importPath.startsWith('..')) return;

      const absImportPath = join(
        dirname(context.getFilename()),
        importPath,
      );

      const newPath = relative(root, absImportPath);

      const replacement = raw.replace(importPath, newPath);

      if (!newPath.startsWith('..')) {
        context.report({
          node,
          messageId: 'noRelativeUntilNecessary',
          data: {newPath},
          fix: fixer => fixer.replaceText(node, replacement),
        });
      }
    },
  }),
};
