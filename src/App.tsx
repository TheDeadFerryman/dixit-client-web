import 'antd/dist/antd.css';

import PerspectiveSwitcher from 'components/app/PerspectiveSwitcher';
import React from 'react';
import { Provider as ReduxProvider } from 'react-redux';
import store from 'redux/store';

const App: React.FC = () => (
	<ReduxProvider store={store}>
		<PerspectiveSwitcher />
	</ReduxProvider>
);

export default App;
