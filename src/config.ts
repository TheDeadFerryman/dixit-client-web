const AppConfig = {
	Api: {
		UserService: 'http://10.242.1.29:8080/api/users',
		LobbyService: 'http://10.242.1.29:8080/api/lobbies',
		NTPSync: 'ntp1.stratum1.ru'
	},
	Gui: {
		Lobby: {
			DefaultPageSize: 20,
			MaxPlayers: 8,

		},
	},
};

export default AppConfig;