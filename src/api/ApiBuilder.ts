import { PromiseOr } from 'util/types';
import { AnyResponse, IdType } from 'jsonrpc';

type ProviderBase = Record<string, unknown>;

export interface ApiAdapter {
	callMethod<P, R, E>(name: string, params: P, id: IdType): PromiseOr<AnyResponse<R, E>>;

	callNotification<P>(name: string, params: P): PromiseOr<void>;
}

type ApiMethod<P, R, E> = (params: P, id: IdType) => PromiseOr<AnyResponse<R, E>>;
type ApiNotification<P> = (params: P) => PromiseOr<void>;

export default class ApiBuilder<T extends ProviderBase> {
	private constructor(private provider: T, private adapter: ApiAdapter) {
	}

	static create(adapter: ApiAdapter): ApiBuilder<ProviderBase> {
		return new ApiBuilder({}, adapter);
	}

	build(): T {
		return this.provider;
	}

	method<N extends string, P, R, E>(name: N): ApiBuilder<T & { [name in N]: ApiMethod<P, R, E> }> {
		const provider = {
			...this.provider,
			[name]: (params: P, id: IdType) => this.adapter.callMethod<P, R, E>(name, params, id),
		} as T & { [name in N]: ApiMethod<P, R, E> };

		return new ApiBuilder(
			provider,
			this.adapter,
		);
	}

	notification<N extends string, P>(name: N): ApiBuilder<T & { [name in N]: ApiNotification<P> }> {
		const provider = {
			...this.provider,
			[name]: (params: P) => this.adapter.callNotification(name, params),
		} as T & { [name in N]: ApiNotification<P> };

		return new ApiBuilder(
			provider,
			this.adapter,
		);
	}
}
