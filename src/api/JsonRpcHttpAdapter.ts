import { ApiAdapter } from 'api/ApiBuilder';
import axios from 'axios';
import { AnyResponse, ErrorResponse, method, notification, ResponseKind, SuccessResponse } from 'jsonrpc';
import { isError, isNil, noop } from 'lodash-es';

export default class JsonRpcHttpAdapter implements ApiAdapter {
	constructor(private readonly apiUrl: string) {
	}

	async callMethod<P, R, E>(name: string, params: P, id: string): Promise<AnyResponse<R, E>> {
		const request = method(name, params, id);

		try {
			const { data } = await axios.post<AnyResponse<R, E>>(this.apiUrl, request);

			if (!isNil((data as ErrorResponse<E>).error)) {
				return {
					...data as ErrorResponse<E>,
					kind: ResponseKind.Error,
				};
			} else {
				return {
					...data as SuccessResponse<R>,
					kind: ResponseKind.Success,
				};
			}
		} catch (e) {
			return {
				kind: ResponseKind.Error,
				jsonrpc: '2.0', id,
				error: {
					code: 1,
					message: isError(e) ? e.message : 'Unknown error',
					data: e as E,
				},
			};
		}
	}

	callNotification<P>(name: string, params: P): Promise<void> {
		const request = notification(name, params);

		return axios.post(this.apiUrl, request).then(noop);
	}
}