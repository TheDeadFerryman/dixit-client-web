import { ApiAdapter } from 'api/ApiBuilder';
import { AnyResponse, IdType, method, ResponseKind } from 'jsonrpc';
import { PromiseOr } from 'util/types';

export default class MockApiAdapter implements ApiAdapter {
	constructor(private readonly mockMap: Record<string, unknown>) {
	}

	callMethod<P, R, E>(name: string, params: P, id: IdType): PromiseOr<AnyResponse<R, E>> {
		const request = method(name, params, id);

		console.log('MockApiAdapter > ', request);

		return {
			id, jsonrpc: '2.0',
			kind: ResponseKind.Success,
			result: this.mockMap[name] as R,
		};
	}

	callNotification<P>(name: string, params: P): PromiseOr<void> {
		return Promise.resolve();
	}

}