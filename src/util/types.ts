export type Nil = null | undefined;

export type Nullable<T> = T | null;
export type Optional<T> = T | undefined;

export type Nilable<T> = Optional<Nullable<T>>;

export type NotNull<T> = T extends null ? never : T;
export type Required<T> = T extends undefined ? never : T;

export type NotNil<T> = Required<NotNull<T>>;

export type PromiseOr<T> = Promise<T> | T;