import { Action } from '@reduxjs/toolkit';

export const NoOpAction: Action<string> = { type: 'noop' };
