import {
	clone,
	sortBy,
} from 'lodash-es';
import SeedRandom from 'seedrandom';

export const shuffle = <T>(collection: T[], seed: string | number): T[] => {
	const random = SeedRandom(seed.toString());

	const res = sortBy(collection);

	for (let i = collection.length - 1; i > 0; i--) {
		const j = Math.abs(random.int32()) % i;

		[res[i], res[j]] = [res[j], res[i]];
	}

	return res;
};

interface TakeOptions<T> {
	collection: T[];
	seed: string | number;
	count: number;
	startFrom: number;
}

export const take = <T>({ collection, count, seed, startFrom }: TakeOptions<T>): T[] => (
	shuffle(collection, seed).slice(startFrom, startFrom + count)
);