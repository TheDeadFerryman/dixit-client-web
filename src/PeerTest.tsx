import React, { ChangeEvent } from 'react';
import Peer, { DataConnection } from 'peerjs';
import { compact, isNil, omit } from 'lodash-es';

interface Message {
	from: string;
	text: string;
}

interface Error {
	source: string;
	data: unknown;
}

type PeerTestProps = Record<string, never>;

interface PeerTestState {
	peerId?: string;
	direct?: string;
	connections: Record<string, DataConnection>;
	messages: Message[];
	errors: Error[];
	text: string;
}

class PeerTest extends React.Component<PeerTestProps, PeerTestState> {
	private readonly peer: Peer = new Peer();

	constructor(props: PeerTestProps) {
		super(props);

		this.state = {
			connections: {},
			messages: [],
			errors: [],
			text: '',
		};
	}

	componentDidMount(): void {
		this.peer.on('open', () => {
			this.setState({
				peerId: this.peer.id,
			});
		});

		this.peer.on('connection', conn => {
			this.bindConnection(conn);

			this.setState(({ connections, ...state }) => ({
				...state,
				connections: { ...connections, [conn.peer]: conn },
			}));
		});

		this.peer.on('error', err => this.setState(({ errors, ...state }) => ({
			...state,
			errors: errors.concat([{
				source: 'self',
				data: err,
			}]),
		})));
	}

	render(): JSX.Element {
		const { peerId, connections, messages, direct, errors } = this.state;

		return (
			<>
				<div><b>Peer id</b>: <kbd>{peerId}</kbd></div>
				<div>
					<b>Connected peers:</b>
					{Object.values(connections).map(conn => (
						<div
							onClick={() => this.onDirectToggled(conn.peer)}
							style={{
								fontWeight: (conn.peer === direct) ? 'bold' : 'normal',
							}}
						>
							<kbd>{conn.peer}</kbd>
						</div>
					))}
					<button onClick={this.onConnectPressed}>
						Connect
					</button>
				</div>
				<div>
					<b>Chat:</b>
					{messages.map(msg => (
						<div
							style={{
								backgroundColor: '#eeeeee',
								padding: 8,
								margin: 4,
							}}
						>
							<b>{msg.from}:</b><br/>
							<pre>{msg.text}</pre>
						</div>
					))}
					<textarea
						placeholder={'Chat here...'}
						onChange={this.onTextChanged}
					/>
					<button onClick={this.onSendPressed}>
						Send
					</button>
				</div>
				<div>
					<b>Errors:</b>
					{errors.map(err => (
						<div style={{
							backgroundColor: '#ffaaaa',
							padding: 8,
							margin: 4,
						}}>
							<b>{err.source}:</b>
							<pre>{JSON.stringify(err.data)}</pre>
						</div>
					))}
				</div>
			</>
		);
	}

	private readonly onConnectPressed = () => {
		const peerId = prompt('Peer id:');

		if (isNil(peerId)) return;

		const conn = this.peer.connect(peerId);

		conn.on('open', () => {
			this.bindConnection(conn);

			this.setState(({ connections, ...state }) => ({
				...state,
				connections: { ...connections, [conn.peer]: conn },
			}));
		});
	};

	private readonly onDirectToggled = (id: string) => {
		const { direct } = this.state;

		if (direct === id) {
			return this.setState({ direct: undefined });
		}

		return this.setState({ direct: id });
	};

	private readonly onTextChanged = (e: ChangeEvent<HTMLTextAreaElement>) =>
		this.setState({
			text: e.target.value,
		});

	private readonly onSendPressed = () => {
		const { connections, direct, text } = this.state;

		const recipients = compact(direct ? [connections[direct]] : Object.values(connections));

		console.log(recipients.map(conn => conn.peer));

		recipients.forEach(conn => conn.send({ text }));
	};

	private readonly bindConnection = (conn: DataConnection) => {
		conn.on('close', () => this.setState(({ connections, messages, ...state }) => ({
			...state,
			connections: omit(connections, conn.peer),
			messages: messages.concat([{
				from: conn.peer,
				text: '<<disconnected>>',
			}]),
		})));

		conn.on('data', data => {
			const msg = this.parseMessage(data, conn);

			this.setState(({ messages, ...state }) => ({
				...state,
				messages: messages.concat([msg]),
			}));
		});

		conn.on('error', err => this.setState(({ errors, ...state }) => ({
			...state,
			errors: errors.concat([{
				source: 'self',
				data: err,
			}]),
		})));
	};

	private readonly parseMessage = (message: Record<string, string>, conn: Peer.DataConnection): Message => ({
		from: conn.peer,
		text: message.text ?? '',
	});
}


export default PeerTest;