export type IdType = string | number;

interface Message {
	jsonrpc: '2.0';
}

interface Request<P> extends Message {
	method: string;
	params: P;
}

export interface Method<P> extends Request<P> {
	id: IdType;
}

export type Notification<P> = Request<P>;

export enum ResponseKind {
	Success,
	Error
}

interface Response extends Message {
	id: IdType;
	kind: ResponseKind;
}

export interface SuccessResponse<R> extends Response {
	result: R;
	kind: ResponseKind.Success;
}

export interface Error<D> {
	code: number;
	message: string;
	data?: D;
}

export interface ErrorResponse<D> extends Response {
	error: Error<D>;
	kind: ResponseKind.Error;
}

export type AnyResponse<R, E> = ErrorResponse<E> | SuccessResponse<R>;

export const method = <P>(method: string, params: P, id: IdType): Method<P> => ({
	jsonrpc: '2.0', method, params, id,
});

export const notification = <P>(method: string, params: P): Notification<P> => ({
	jsonrpc: '2.0', method, params,
});

// class ParseError extends Error {}
