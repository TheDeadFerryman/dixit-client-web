import { AppReducer } from 'components/app/redux';
import { UserReducer } from 'components/app/perspective/User/redux';
import { GameReducer } from 'components/app/perspective/Game/redux';

const reducers = ({
	app: AppReducer,
	user: UserReducer,
	game: GameReducer,
});

export default reducers;
