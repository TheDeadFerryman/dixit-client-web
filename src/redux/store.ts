import {
	Action,
	configureStore,
	ThunkAction,
} from '@reduxjs/toolkit';
import { GameState } from 'components/app/perspective/Game/redux';
import { UserState } from 'components/app/perspective/User/redux';
import { AppState } from 'components/app/redux';
import {
	FLUSH,
	PAUSE,
	PERSIST,
	PURGE,
	REGISTER,
	REHYDRATE,
} from 'redux-persist';
import ReduxThunk from 'redux-thunk';
import reducers from 'redux/reducers';
import { PromiseOr } from 'util/types';

export interface RootState {
	app: AppState;
	user: UserState;
	game: GameState;
}

export type AppAction = Action<string>;

const store = configureStore({
	reducer: reducers,
	devTools: process.env.NODE_ENV !== 'production',
	middleware: getDefaultMiddleware => getDefaultMiddleware({
		serializableCheck: {
			ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
		},
		immutableCheck: false,
	})
		.concat(ReduxThunk),
});

export type AppDispatch = typeof store.dispatch;

type AppThunkReturnType = PromiseOr<AppAction>;
export type AppThunk<E = void> = ThunkAction<AppThunkReturnType, RootState, E, AppAction>;

export default store;