import {
	GlobalOutlined,
	TeamOutlined,
} from '@ant-design/icons';
import {
	Alert,
	Button,
	List,
	Pagination,
} from 'antd';
import JoinLobbyModal from 'components/app/perspective/LobbyList/view/JoinLobbyModal';
import LobbyListActions from 'components/app/perspective/LobbyList/view/LobbyListActions';
import IconText from 'components/util/IconText';
import AppConfig from 'config';
import { isEmpty } from 'lodash-es';
import { Component } from 'react';
import {
	CreateLobbyData,
	LobbyError,
	LobbyList,
	LobbyQuery,
	LobbyRegion,
} from './api';

export type QueryFragment = Pick<LobbyQuery, 'searchQuery' | 'region'>;

interface LobbyListViewProps {
	loading: boolean;
	error?: LobbyError;
	lobbies: LobbyList;

	onQueryChanged(query: LobbyQuery): unknown;

	onJoinLobby(id: string, password: string): unknown;

	onCreateLobby(lobby: CreateLobbyData): unknown;
}

interface LobbyListViewState extends LobbyQuery {
	selectedLobbyId?: string;
}

class LobbyListView extends Component<LobbyListViewProps, LobbyListViewState> {
	constructor(props: LobbyListViewProps) {
		super(props);

		this.state = {
			searchQuery: '',
			region: LobbyRegion.Europe,
			page: 0,
			pageSize: AppConfig.Gui.Lobby.DefaultPageSize,
		};
	}

	componentDidMount(): void {
		this.onQueryChanged();
	}

	private readonly onQueryChanged = () => this.props.onQueryChanged(this.state);

	private readonly onLobbySelected = (lobbyId?: string) => this.setState({ selectedLobbyId: lobbyId });

	private readonly onPageChanged = (
		page: number,
		pageSize: number = AppConfig.Gui.Lobby.DefaultPageSize,
	) => this.setState({ page, pageSize }, this.onQueryChanged);

	private readonly onQueryFragmentChanged = (queryFragment: QueryFragment) =>
		this.setState(queryFragment, this.onQueryChanged);

	render(): JSX.Element {
		const { error, loading, lobbies, onJoinLobby, onCreateLobby } = this.props;
		const { page, pageSize, searchQuery, region, selectedLobbyId } = this.state;

		const maxPlayers = AppConfig.Gui.Lobby.MaxPlayers;

		return (
			<div style={{ width: '100vw' }}>
				<JoinLobbyModal
					lobbyId={selectedLobbyId}
					onJoin={onJoinLobby}
					onClose={() => this.onLobbySelected(undefined)}
				/>
				{error && (
					<Alert
						showIcon
						type={'error'}
						message={`Failed to fetch lobbies: ${error.message}`}
					/>
				)}
				<List
					loading={loading}
					header={<LobbyListActions
						onQuerySubmitted={this.onQueryFragmentChanged}
						onCreateLobby={onCreateLobby}
						initial={{ searchQuery: searchQuery, region }}
					/>}
					itemLayout={'horizontal'}
					dataSource={lobbies.lobbies}
					locale={{ emptyText: 'There are no lobbies now!' }}
					renderItem={lobby => (
						<List.Item
							actions={[
								<IconText icon={TeamOutlined} text={`${lobby.players} / ${maxPlayers}`} />,
								<IconText icon={GlobalOutlined} text={LobbyRegion[lobby.region]} />,
							]}
						>
							<List.Item.Meta title={(
								<Button
									type={'link'}
									onClick={() => this.onLobbySelected(lobby.id)}
								>
									{lobby.name}
								</Button>
							)} />
						</List.Item>
					)}
				/>
				<Pagination
					current={page}
					showSizeChanger
					pageSize={pageSize}
					total={lobbies.total}
					disabled={isEmpty(lobbies.lobbies)}
					defaultPageSize={AppConfig.Gui.Lobby.DefaultPageSize}
					onChange={this.onPageChanged}
				/>
			</div>
		);
	}
}

export default LobbyListView;