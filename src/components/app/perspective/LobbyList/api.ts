import ApiBuilder from 'api/ApiBuilder';
import JsonRpcHttpAdapter from 'api/JsonRpcHttpAdapter';
import AppConfig from 'config';

export enum LobbyRegion {
	Asia,
	Europe,
	Africa,
	NorthAmerica,
	SouthAmerica,
	Australia,
}

export interface LobbyQuery {
	searchQuery: string;
	region: LobbyRegion;
	page: number;
	pageSize: number;
}

export interface LobbyError {
	code: number;
	message: string;
}

export interface LobbyResult {
	id: string;
	name: string;
	region: LobbyRegion;
	players: number;
}

export interface LobbyList {
	lobbies: LobbyResult[];
	total: number;
}

export const EmptyLobbyList: LobbyList = {
	lobbies: [],
	total: 0,
};

export interface CreateLobbyData {
	name: string;
	password: string;
	region: LobbyRegion;
}

export interface LobbyCredentials {
	lobbyId: string;
	password: string;
}

export interface JoinLobbyData extends LobbyCredentials {
	userId: string;
	connector: string;
}

export interface LobbyPeer {
	userId: string;
	alias: string;
	connector: string;
}

export type JoinLobbyResult = LobbyPeer[];

const LobbyApiAdapter = new JsonRpcHttpAdapter(AppConfig.Api.LobbyService);

const LobbyApi = ApiBuilder.create(LobbyApiAdapter)
	.method<'find', LobbyQuery, LobbyList, void>('find')
	.method<'create', CreateLobbyData, string, void>('create')
	.method<'join', JoinLobbyData, JoinLobbyResult, void>('join')
	.method<'validatePassword', LobbyCredentials, boolean, void>('validatePassword')
	.method<'start', { lobbyId: string; }, number, void>('start')
	.build();

export default LobbyApi;