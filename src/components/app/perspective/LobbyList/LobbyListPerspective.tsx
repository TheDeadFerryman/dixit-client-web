import {
	Modal,
	notification,
} from 'antd';
import LobbyApi, {
	CreateLobbyData,
	EmptyLobbyList,
	LobbyError,
	LobbyList,
	LobbyQuery,
} from 'components/app/perspective/LobbyList/api';
import LobbyListView from 'components/app/perspective/LobbyList/LobbyListView';
import { User } from 'components/app/perspective/User/redux';
import { AppActions } from 'components/app/redux';
import { ResponseKind } from 'jsonrpc';
import {
	isError,
	uniqueId,
} from 'lodash-es';
import { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import {
	AppDispatch,
	RootState,
} from 'redux/store';

interface LobbyListPerspectiveProps {
	dispatch: AppDispatch;
	me?: User;
}

interface LobbyListPerspectiveState {
	loading: boolean;
	error?: LobbyError;
	lobbies: LobbyList;
}

class LobbyListPerspective extends Component<LobbyListPerspectiveProps, LobbyListPerspectiveState> {
	constructor(props: LobbyListPerspectiveProps) {
		super(props);

		this.state = {
			loading: false,
			lobbies: {
				lobbies: [],
				total: 0,
			},
		};
	}

	private readonly onQueryChanged = async (query: LobbyQuery) => {
		this.setState({ loading: true });
		try {
			const resp = await LobbyApi.find(query, uniqueId());

			if (resp.kind === ResponseKind.Error) {
				this.setState({
					error: resp.error,
					lobbies: EmptyLobbyList,
					loading: false,
				});
			} else {
				this.setState({
					error: undefined,
					lobbies: resp.result,
					loading: false,
				});
			}
		} catch (e) {
			this.setState({
				error: {
					code: 3,
					message: isError(e) ? e.message : 'Internal error',
				},
				lobbies: EmptyLobbyList,
				loading: false,
			});
		}
	};

	private readonly onCreateLobby = async (data: CreateLobbyData) => {
		try {
			const rsp = await LobbyApi.create(data, uniqueId());

			if (rsp.kind === ResponseKind.Error) {
				return Modal.error({
					title: 'Error',
					content: `Failed to create lobby: ${rsp.error.message}`,
				});
			} else {
				return notification.success({
					message: 'Lobby created!',
					duration: 5000,
				});
			}
		} catch (e) {
			const message = isError(e) ? e.message : 'Internal error';

			return Modal.error({
				title: 'Error',
				content: `Failed to create lobby: ${message}`,
			});
		}
	};

	private readonly onJoinLobby = (lobbyId: string, password: string): unknown =>
		this.props.dispatch(AppActions.initGame({ lobbyId, password }));

	render() {
		const { error, loading, lobbies } = this.state;

		return <LobbyListView
			error={error}
			loading={loading}
			lobbies={lobbies}
			onQueryChanged={this.onQueryChanged}
			onCreateLobby={this.onCreateLobby}
			onJoinLobby={this.onJoinLobby}
		/>;
	}
}

export default compose(
	connect(
		({ user: { me } }: RootState) => ({ me }),
		dispatch => ({ dispatch }),
	),
)(LobbyListPerspective);