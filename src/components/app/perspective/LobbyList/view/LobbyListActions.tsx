import { PlusSquareOutlined } from '@ant-design/icons';
import { Button, Col, Form, Input, Row, Select } from 'antd';
import { CreateLobbyData, LobbyRegion } from 'components/app/perspective/LobbyList/api';
import { QueryFragment } from 'components/app/perspective/LobbyList/LobbyListView';
import CreateLobbyModal from 'components/app/perspective/LobbyList/view/CreateLobbyModal';
import { FC, useState } from 'react';
import enumKeys from 'util/enumKeys';

interface LobbyListActionsProps {
	initial: QueryFragment;

	onQuerySubmitted(query: QueryFragment): unknown;

	onCreateLobby(data: CreateLobbyData): unknown;
}

const LobbyListActions: FC<LobbyListActionsProps> = ({ initial, onQuerySubmitted, onCreateLobby }) => {
	const [form] = Form.useForm<QueryFragment>();
	const [modal, toggleModal] = useState(false);

	return (
		<Row>
			<Col span={22}>
				<Form
					form={form}
					layout={'inline'}
					initialValues={initial}
					onFinish={onQuerySubmitted}
				>
					<Form.Item name={'searchQuery'}>
						<Input.Search
							placeholder={'Lobby name'}
							onSearch={form.submit}
						/>
					</Form.Item>
					<Form.Item name={'region'}>
						<Select onChange={form.submit}>
							{enumKeys(LobbyRegion).map(key => (
								<Select.Option value={LobbyRegion[key]}>
									{key}
								</Select.Option>
							))}
						</Select>
					</Form.Item>
					<Form.Item>
					</Form.Item>
				</Form>
			</Col>
			<Col span={2}>
				<Button
					block
					type={'primary'}
					icon={<PlusSquareOutlined />}
					onClick={() => toggleModal(true)}
				>
					Create lobby
				</Button>
				<CreateLobbyModal
					opened={modal}
					onClose={() => toggleModal(false)}
					onCreateLobby={onCreateLobby}
				/>
			</Col>
		</Row>
	);
};

export default LobbyListActions;