import { Form, Input, Modal, Select } from 'antd';
import { CreateLobbyData, LobbyRegion } from 'components/app/perspective/LobbyList/api';
import { FC } from 'react';
import enumKeys from 'util/enumKeys';


interface CreateLobbyModalProps {
	opened: boolean;

	onClose(): unknown;

	onCreateLobby(data: CreateLobbyData): unknown;
}

const CreateLobbyModal: FC<CreateLobbyModalProps> = ({ opened, onClose, onCreateLobby }) => {
	const [form] = Form.useForm<CreateLobbyData>();

	const onFinish = (data: CreateLobbyData) => {
		onCreateLobby(data);
		onClose();
	};

	return (
		<Modal
			closable
			visible={opened}
			onOk={form.submit}
			onCancel={onClose}
			okText={'Create lobby'}
			title={'Creating a lobby'}
		>
			<Form
				form={form}
				layout={'vertical'}
				onFinish={onFinish}
			>
				<Form.Item
					rules={[{ required: true }]}
					name={'name'}
					label={'Lobby name:'}
				>
					<Input />
				</Form.Item>
				<Form.Item
					rules={[{ required: true }]}
					name={'region'}
					label={'Region'}
				>
					<Select>
						{enumKeys(LobbyRegion).map(key => (
							<Select.Option value={LobbyRegion[key]}>
								{key}
							</Select.Option>
						))}
					</Select>
				</Form.Item>
				<Form.Item
					rules={[{ required: true }]}
					name={'password'}
					label={'Passkey'}
				>
					<Input />
				</Form.Item>
			</Form>
		</Modal>
	);
};

export default CreateLobbyModal;