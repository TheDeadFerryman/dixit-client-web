import {
	Form,
	Input,
	Modal,
} from 'antd';
import LobbyApi from 'components/app/perspective/LobbyList/api';
import { ResponseKind } from 'jsonrpc';
import {
	isNil,
	uniqueId,
} from 'lodash-es';
import {
	FC,
	useState,
} from 'react';

interface JoinLobbyModalProps {
	lobbyId?: string;

	onClose(): unknown;

	onJoin(lobbyId: string, password: string): unknown;
}

interface FormData {
	password: string;
}

const JoinLobbyModal: FC<JoinLobbyModalProps> = ({ onClose, onJoin, lobbyId }) => {
	const [form] = Form.useForm<FormData>();

	const validator = async (password: string): Promise<void | Error> => {
		if (isNil(lobbyId)) return new Error('Couldn\'t find lobby');

		try {
			const rsp = await LobbyApi.validatePassword({ lobbyId, password }, uniqueId());

			if (rsp.kind === ResponseKind.Error) {
				return new Error(rsp.error.message);
			} else {
				if (rsp.result) {
					return;
				} else {
					return new Error('Invalid password!');
				}
			}
		} catch (e) {
			return new Error('Internal error');
		}

	};

	const onFormFinished = ({ password }: FormData) => (
		lobbyId && onJoin(lobbyId, password)
	);

	return (
		<Modal
			closable
			okButtonProps={{ loading: form.isFieldValidating('password') }}
			onCancel={onClose}
			visible={!!lobbyId}
			title={'Join lobby'}
			onOk={() => form.submit()}
		>
			<Form
				form={form}
				initialValues={{ password: '' }}
				onFinish={onFormFinished}
			>
				<Form.Item
					name={'password'}
					label={'Lobby passkey'}
					rules={[
						{ required: true },
						{ validator: (_, value) => validator(value) },
					]}
				>
					<Input />
				</Form.Item>
			</Form>
		</Modal>
	);
};

export default JoinLobbyModal;