import { createSlice } from '@reduxjs/toolkit';
import USER_ACTIONS from 'components/app/perspective/User/actions';

export interface User {
	alias: string;
	id: string;
}

export interface UserError {
	code: number;
	message: string;
}

export interface UserState {
	me?: User;
	error?: UserError;
	loading: boolean;
}

const initialState: UserState = {
	loading: false,
};

const { reducer, actions } = createSlice({
	name: 'UserActions',
	initialState,
	reducers: {
		...USER_ACTIONS.actions,
	},
});

const thunkActions = {
	...USER_ACTIONS.thunkActions,
};

export const UserActions = {
	...actions,
	...thunkActions,
};

export const UserReducer = reducer;