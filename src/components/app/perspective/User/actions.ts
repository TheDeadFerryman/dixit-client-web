import { PayloadAction } from '@reduxjs/toolkit';
import UserApi, { UserData } from 'components/app/perspective/User/api';
import { User, UserActions, UserError, UserState } from 'components/app/perspective/User/redux';
import { ResponseKind } from 'jsonrpc';
import { uniqueId } from 'lodash-es';
import { AppAction, AppThunk } from 'redux/store';

const actions = {
	setUserLoadingInternal: (
		state: UserState,
		{ payload }: PayloadAction<boolean>,
	): UserState => ({
		...state,
		loading: payload,
	}),
	setUserInternal: (
		state: UserState,
		{ payload }: PayloadAction<User>,
	): UserState => ({
		...state,
		me: payload,
		error: undefined,
	}),
	setUserErrorInternal: (
		state: UserState,
		{ payload }: PayloadAction<UserError>,
	): UserState => ({
		...state,
		me: undefined,
		error: payload,
	}),
	resetUserError: (
		state: UserState,
	): UserState => ({
		...state,
		error: undefined,
	}),
	logoutUser: (
		state: UserState,
	): UserState => ({
		...state,
		me: undefined,
		error: undefined,
	}),
};

const thunkActions = {
	loginUser: (user: UserData): AppThunk => (
		async (dispatch): Promise<AppAction> => {
			try {
				dispatch(UserActions.setUserLoadingInternal(true));

				const response = await UserApi.login(user, uniqueId());

				if (response.kind === ResponseKind.Error) {
					return dispatch(UserActions.setUserErrorInternal(response.error));
				} else {
					return dispatch(UserActions.setUserInternal({
						alias: user.alias,
						id: response.result,
					}));
				}
			} finally {
				dispatch(UserActions.setUserLoadingInternal(false));
			}
		}
	),
	registerUser: (user: UserData): AppThunk => (
		async (dispatch): Promise<AppAction> => {
			try {
				const response = await UserApi.register(user, uniqueId());

				console.log(response);

				if (response.kind === ResponseKind.Error) {
					return dispatch(UserActions.setUserErrorInternal(response.error));
				} else {
					return dispatch(UserActions.setUserInternal({
						alias: user.alias,
						id: response.result,
					}));
				}
			} finally {
				dispatch(UserActions.setUserLoadingInternal(false));
			}
		}
	),
};

const USER_ACTIONS = { actions, thunkActions };

export default USER_ACTIONS;