import { Alert, Button, Form, Input } from 'antd';
import { UserError } from 'components/app/perspective/User/redux';
import { FC } from 'react';

enum SubmitType {
	Login,
	Register
}

interface UserForm {
	type: SubmitType;
	alias: string;
	password: string;
}

interface UserViewProps {
	error?: UserError;

	loading: boolean;

	onLogin(data: Omit<UserForm, 'type'>): unknown;

	onRegister(data: Omit<UserForm, 'type'>): unknown;

	onErrorDismissed(): unknown;
}

const UserView: FC<UserViewProps> = ({ onLogin, onRegister, onErrorDismissed, error, loading }) => {
	const [form] = Form.useForm<UserForm>();

	const onBeforeSubmit = (type: SubmitType) => {
		form.setFieldsValue({ type });
		form.submit();
	};

	const onSubmit = ({ alias, password, type }: UserForm) => {
		switch (type) {
		case SubmitType.Login:
			return onLogin({ alias, password });
		case SubmitType.Register:
			return onRegister({ alias, password });
		default:
			return;
		}
	};

	return (
		<div>
			<div>
				{error && (
					<Alert
						closable
						showIcon
						type={'error'}
						message={error.message}
						onClose={onErrorDismissed}
					/>
				)}
				<Form
					form={form}
					labelCol={{ span: 8 }}
					wrapperCol={{ span: 8 }}
					onFinish={onSubmit}
				>
					<Form.Item name={'type'}>
						<Input hidden/>
					</Form.Item>
					<Form.Item
						required
						name={'alias'}
						label={'Alias'}
					>
						<Input/>
					</Form.Item>
					<Form.Item
						required
						name={'password'}
						label={'Password'}
					>
						<Input.Password/>
					</Form.Item>
					<Form.Item
						wrapperCol={{
							offset: 8,
							span: 16,
						}}
					>
						<Button.Group>
							<Button
								type={'primary'}
								onClick={() => onBeforeSubmit(SubmitType.Login)}
							>
								Login
							</Button>
							<Button
								danger type={'primary'}
								onClick={() => onBeforeSubmit(SubmitType.Register)}
							>
								Register
							</Button>
						</Button.Group>
					</Form.Item>
				</Form>
			</div>
		</div>
	);
};

export default UserView;