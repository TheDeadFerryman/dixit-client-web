import { UserData } from 'components/app/perspective/User/api';
import { User, UserActions, UserError } from 'components/app/perspective/User/redux';
import UserView from 'components/app/perspective/User/UserView';
import { AppActions, AppPerspective } from 'components/app/redux';
import { isNil } from 'lodash-es';
import { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { AppDispatch, RootState } from 'redux/store';

interface UserPerspectiveProps {
	me?: User;
	error?: UserError;
	loading: boolean;
	dispatch: AppDispatch;
}

class UserPerspective extends Component<UserPerspectiveProps> {
	componentDidUpdate() {
		const { me, dispatch } = this.props;

		if (!isNil(me)) {
			void dispatch(AppActions.selectPerspective(AppPerspective.LobbyList));
		}
	}

	render() {
		const { error, loading } = this.props;

		return (
			<UserView
				loading={loading}
				error={error}
				onLogin={this.onUserLogin}
				onRegister={this.onUserRegister}
				onErrorDismissed={this.onErrorDismissed}
			/>
		);
	}

	private readonly onUserLogin = (user: UserData) => this.props.dispatch(UserActions.loginUser(user));
	private readonly onUserRegister = (user: UserData) => this.props.dispatch(UserActions.registerUser(user));
	private readonly onErrorDismissed = () => this.props.dispatch(UserActions.resetUserError());
}

export default compose(
	connect(
		({ user }: RootState) => (user),
		dispatch => ({ dispatch }),
	),
)(UserPerspective);