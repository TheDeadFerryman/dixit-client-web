import ApiBuilder from 'api/ApiBuilder';
import JsonRpcHttpAdapter from 'api/JsonRpcHttpAdapter';
import AppConfig from 'config';

export interface UserData {
	alias: string;
	password: string;
}

const UserApiAdapter = new JsonRpcHttpAdapter(AppConfig.Api.UserService);

const UserApi = ApiBuilder
	.create(UserApiAdapter)
	.method<'login', UserData, string, void>('login')
	.method<'register', UserData, string, void>('register')
	.build();

export default UserApi;