import { GameStage } from 'components/app/perspective/Game/redux';
import GuessesStage from 'components/app/perspective/Game/stage/GuessesStage';
import NarrationStage from 'components/app/perspective/Game/stage/NarratorStage';
import PreGameStage from 'components/app/perspective/Game/stage/PreGameStage';
import ResultsStage from 'components/app/perspective/Game/stage/ResultsStage';
import RevealsStage from 'components/app/perspective/Game/stage/RevealsStage';
import { FC } from 'react';
import { useSelector } from 'react-redux';
import { RootState } from 'redux/store';

type GameProps = Record<string, unknown>;

const GamePerspective: FC<GameProps> = () => {
	const stage = useSelector(({ game: { stage } }: RootState) => stage);

	switch (stage) {
	case GameStage.PreGame:
		return <PreGameStage />;
	case GameStage.Narration:
		return <NarrationStage />;
	case GameStage.Guesses:
		return <GuessesStage />;
	case GameStage.Reveals:
		return <RevealsStage />;
	case GameStage.Results:
		return <ResultsStage />;
	case GameStage.Election:
	default:
		return null;
	}
};

export default GamePerspective;
