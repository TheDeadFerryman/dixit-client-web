import {
	GreetMessage,
	GuessMessage,
	NextRoundMessage,
	OfferMessage,
	ReadyMessage,
	ReadyNextMessage,
	RevealMessage,
	RevealStartedMessage,
	StartedMessage,
	StartingMessage,
} from 'components/app/perspective/Game/messaging/types';
import {
	DeckCardId,
	Offer,
} from 'components/app/perspective/Game/redux';
import { User } from 'components/app/perspective/User/redux';

export const createGreeter = (me: User): GreetMessage => ({
	jsonrpc: '2.0',
	method: 'greet',
	params: me,
});

export const createReady = (): ReadyMessage => ({
	jsonrpc: '2.0',
	method: 'ready',
	params: undefined,
});

export const createReadyNext = (): ReadyNextMessage => ({
	jsonrpc: '2.0',
	method: 'readyNext',
	params: undefined,
});

export const createStarting = (): StartingMessage => ({
	jsonrpc: '2.0',
	method: 'starting',
	params: undefined,
});

export const createStarted = (seed: number): StartedMessage => ({
	jsonrpc: '2.0',
	method: 'started',
	params: seed,
});

export const createRevealStarted = (): RevealStartedMessage => ({
	jsonrpc: '2.0',
	method: 'revealStarted',
	params: undefined,
});

export const createReveal = (id: string): RevealMessage => ({
	jsonrpc: '2.0',
	method: 'reveal',
	params: id,
});

export const createDesc = (desc: Offer): OfferMessage => ({
	jsonrpc: '2.0',
	method: 'offer',
	params: desc,
});

export const createGuess = (desc: DeckCardId): GuessMessage => ({
	jsonrpc: '2.0',
	method: 'guess',
	params: desc,
});

export const createNextRound = (lastOrdinal: number): NextRoundMessage => ({
	jsonrpc: '2.0',
	method: 'nextRound',
	params: lastOrdinal,
});