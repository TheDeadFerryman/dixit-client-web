import { createStarted } from 'components/app/perspective/Game/messaging/index';
import {
	AnyMessage,
	GreetMessage,
	GuessMessage,
	NextRoundMessage,
	OfferMessage,
	ReadyMessage,
	ReadyNextMessage,
	RevealMessage,
	RevealStartedMessage,
	StartedMessage,
} from 'components/app/perspective/Game/messaging/types';
import {
	DeckCardId,
	GameActions,
	GamePlayer,
	GameStage,
} from 'components/app/perspective/Game/redux';
import { User } from 'components/app/perspective/User/redux';
import {
	isNil,
	round,
} from 'lodash-es';
import { DataConnection } from 'peerjs';
import {
	AppDispatch,
	RootState,
} from 'redux/store';
import deck from 'resources/deck.json';
import {
	shuffle,
	take,
} from 'util/collections';
import {
	Optional,
	PromiseOr,
} from 'util/types';

type StateGetter = () => RootState;

export default class MessageHandler {
	static getUserOrdinals(me: Optional<User>, players: Record<string, GamePlayer>, seed: number): Record<string, number> {
		if (isNil(me)) throw new Error('illegal state');

		const pIds = Object.keys(players).concat(me.id);

		console.log(pIds);

		return Object.fromEntries(
			shuffle(pIds, seed).map((v, i) => [v, i]),
		);
	}

	constructor(private readonly dispatch: AppDispatch, private readonly getState: StateGetter) {
	}

	handle(message: AnyMessage, conn: DataConnection): PromiseOr<void> {
		switch (message.method) {
		case 'greet':
			return this.handleGreet(message, conn);
		case 'ready':
			return this.handleReady(message, conn);
		case 'readyNext':
			return this.handleReadyNext(message, conn);
		case 'started':
			return this.handleStarted(message, conn);
		case 'offer':
			return this.handleOffer(message, conn);
		case 'guess':
			return this.handleGuess(message, conn);
		case 'revealStarted':
			return this.handleRevealStarted(message, conn);
		case 'reveal':
			return this.handleReveal(message, conn);
		case 'nextRound':
			return this.handleNextRound(message, conn);
		}
	}

	handleClosed(userId: string, conn: DataConnection): void {
		const { game: { narratorOrdinal } } = this.getState();

		const ordinals = this.getPlayersOrder();

		this.dispatch(GameActions.togglePeerActive([userId, false]));

		if (ordinals[userId] === narratorOrdinal) {
			void this.nextNarrator();
		}
	}

	private async handleNextRound(message: NextRoundMessage, conn: DataConnection) {
		const { game: { roundOrdinal } } = this.getState();

		if (roundOrdinal !== message.params) return;

		await this.dispatch(GameActions.refreshHand());
		await this.dispatch(GameActions.nextNarrator());

		this.dispatch(GameActions.setNextRound());
		this.dispatch(GameActions.setStage(GameStage.Narration));
	}

	private handleReadyNext(message: ReadyNextMessage, conn: DataConnection) {
		const { game: { players } } = this.getState();

		const res = Object.entries(players).find(([, player]) => (player.peer.peer === conn.peer));

		if (isNil(res)) return;

		this.dispatch(GameActions.addReadyNext([res[0], true]));

		void this.dispatch(GameActions.checkReady());
	}

	private handleReveal(message: RevealMessage, conn: DataConnection) {
		const { game: { players } } = this.getState();

		const res = Object.entries(players).find(([, player]) => (player.peer.peer === conn.peer));

		if (isNil(res)) return;

		this.dispatch(GameActions.addReveal([res[0], message.params]));

		void this.dispatch(GameActions.checkReveals());
	}

	private handleRevealStarted(message: RevealStartedMessage, conn: DataConnection) {
		const { game: { stage } } = this.getState();

		if (stage !== GameStage.Guesses) return;

		this.dispatch(GameActions.setStage(GameStage.Reveals));
	}

	private handleReady(message: ReadyMessage, conn: DataConnection) {
		const { game: { players } } = this.getState();

		const player = Object.entries(players).find(([, player]) => (
			player.peer.peer === conn.peer
		));

		if (isNil(player)) return;

		this.dispatch(GameActions.togglePeerReady([player[0], true]));
	}

	private handleGreet(message: GreetMessage, conn: DataConnection) {
		const peer = {
			userId: message.params.id,
			alias: message.params.alias,
			connector: conn.peer,
		};

		this.dispatch(GameActions.addPlayerPeer([peer, conn]));
		this.dispatch(GameActions.togglePeerActive([peer.userId, true]));

		conn.on('close', () => this.handleClosed(peer.userId, conn));
		conn.on('error', err => this.dispatch(
			GameActions.setError(err),
		));

	}

	private handleStarted(message: StartedMessage, conn: DataConnection) {
		this.dispatch(GameActions.setSeed(message.params));

		return this.startGame();
	}

	startGame() {
		const { game: { stage } } = this.getState();

		if (stage !== GameStage.PreGame) return;

		const initialHand = this.getMyInitialHand();

		this.dispatch(GameActions.setStage(GameStage.Narration));

		this.dispatch(GameActions.setMyHand(initialHand));
		this.dispatch(GameActions.setTaken(initialHand.length));
	}

	private handleOffer(message: OfferMessage, conn: DataConnection) {
		const { game: { narratorOrdinal }, user: { me } } = this.getState();

		if (isNil(me)) return;

		const ordinals = this.getPlayersOrder();

		if (narratorOrdinal === ordinals[me.id]) return;

		this.dispatch(GameActions.setStage(GameStage.Guesses));
		this.dispatch(GameActions.setOffer(message.params));
	}


	private nextNarrator() {
		return this.dispatch(GameActions.nextNarrator());
	}

	private getPlayersOrder(): Record<string, number> {
		const { game: { players, seed }, user: { me } } = this.getState();

		return MessageHandler.getUserOrdinals(me, players, seed);
	}

	private getMyInitialHand(): DeckCardId[] {
		const { game: { players, seed }, user: { me } } = this.getState();

		if (isNil(me)) throw new Error('illegal state');

		const cardKeys = Object.keys(deck) as DeckCardId[];
		const ordinals = this.getPlayersOrder();

		const shift = round(cardKeys.length / (Object.keys(players).length + 1)) * ordinals[me.id];

		return take({
			collection: cardKeys,
			startFrom: shift,
			count: 4, seed,
		});
	}

	private handleGuess(message: GuessMessage, conn: DataConnection) {
		const { game: { players }, user: { me } } = this.getState();

		if (isNil(me)) return;

		const res = Object.entries(players).find(([, player]) => (player.peer.peer === conn.peer));

		if (isNil(res)) return;

		this.dispatch(GameActions.addGuess([res[0], message.params]));

		void this.dispatch(GameActions.checkGuesses());
	}
}