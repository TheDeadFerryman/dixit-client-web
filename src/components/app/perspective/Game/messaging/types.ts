import {
	DeckCardId,
	Offer,
} from 'components/app/perspective/Game/redux';
import { User } from 'components/app/perspective/User/redux';
import { Notification } from 'jsonrpc';

export interface GreetMessage extends Notification<User> {
	method: 'greet';
}

export interface OfferMessage extends Notification<Offer> {
	method: 'offer';
}

export interface ReadyMessage extends Notification<undefined> {
	method: 'ready';
}

export interface ReadyNextMessage extends Notification<undefined> {
	method: 'readyNext';
}

export interface StartingMessage extends Notification<undefined> {
	method: 'starting';
}

export interface StartedMessage extends Notification<number> {
	method: 'started';
}

export interface RevealStartedMessage extends Notification<undefined> {
	method: 'revealStarted';
}

export interface RevealMessage extends Notification<string> {
	method: 'reveal';
}

export interface GuessMessage extends Notification<DeckCardId> {
	method: 'guess';
}

export interface NextRoundMessage extends Notification<number> {
	method: 'nextRound';
}

export type AnyMessage = GreetMessage | ReadyMessage | StartedMessage
	| StartingMessage | OfferMessage | GuessMessage | RevealStartedMessage
	| RevealMessage | ReadyNextMessage | NextRoundMessage;