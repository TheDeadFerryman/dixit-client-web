import { Image } from 'antd';
import MessageHandler from 'components/app/perspective/Game/messaging/MessageHandler';
import { GameActions } from 'components/app/perspective/Game/redux';
import {
	isEmpty,
	isNil,
	noop,
} from 'lodash-es';
import { FC } from 'react';
import {
	useDispatch,
	useSelector,
} from 'react-redux';
import {
	AppDispatch,
	RootState,
} from 'redux/store';
import deck from 'resources/deck.json';

interface NarratorStageProps {
	[key: string]: unknown;
}

const ResultsStage: FC<NarratorStageProps> = () => {
	const {
		game: { narratorOrdinal, players, seed, currentOffer, guesses, reveals, readyNext },
		user: { me },
	} = useSelector(({ game, user }: RootState) => ({
		game,
		user,
	}));

	const dispatch = useDispatch<AppDispatch>();

	if (isNil(me)) return <>Illegal state!</>;

	if (isNil(currentOffer)) return <>Illegel state!</>;

	const ordinals = MessageHandler.getUserOrdinals(me, players, seed);

	console.log(ordinals);

	const meNarrator = ordinals[me.id] === narratorOrdinal;

	const narratorIdF = Object.entries(ordinals).find(([, ord]) => ord === narratorOrdinal);
	const narratorId = meNarrator ? me.id : (isEmpty(narratorIdF) ? '' : (narratorIdF || [''])[0]);

	const onNextRound = () => (
		void dispatch(GameActions.readyNext())
	);

	return (
		<div>
			<pre>
				Proposed by: {meNarrator ? me?.alias : players[narratorId]?.alias} <br />
				Chosen by:
				{Object.entries(reveals).filter(([, reveal]) => (
					reveal === narratorId
				)).map(([user]) => (me.id === user ? me?.alias : players[user]?.alias)).join(', ')}
			</pre>
			<Image
				src={`/cards/${deck[currentOffer.cardId]}`}
				style={{
					cursor: 'pointer',
					height: 200,
					width: 'auto',
				}}
			/>
			{Object.entries(guesses).map(([userId, guess]) => (
				<>
					<pre>
						Proposed by: {userId === me.id ? me?.alias : players[userId]?.alias} <br />
						Chosen by:
						{Object.entries(reveals).filter(([, reveal]) => (
							reveal === userId
						)).map(([user]) => (me.id === user ? me?.alias : players[user]?.alias)).join(', ')}
					</pre>
					<Image
						src={`/cards/${deck[guess]}`}
						style={{
							cursor: 'pointer',
							height: 200,
							width: 'auto',
						}}
					/>
				</>
			))}
			<button
				disabled={readyNext[me.id]}
				onClick={onNextRound}
			>
				Next round
			</button>
		</div>
	);
};

export default ResultsStage;