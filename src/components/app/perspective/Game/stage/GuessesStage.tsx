import { Image } from 'antd';
import MessageHandler from 'components/app/perspective/Game/messaging/MessageHandler';
import {
	DeckCardId,
	GameActions,
} from 'components/app/perspective/Game/redux';
import {
	isNil,
	noop,
} from 'lodash-es';
import { FC } from 'react';
import {
	useDispatch,
	useSelector,
} from 'react-redux';
import {
	AppDispatch,
	RootState,
} from 'redux/store';
import deck from 'resources/deck.json';

interface NarratorStageProps {
	[key: string]: unknown;
}

const GuessesStage: FC<NarratorStageProps> = () => {
	const {
		game: { myHand, narratorOrdinal, players, seed, currentOffer, guesses },
		user: { me },
	} = useSelector(({ game, user }: RootState) => ({
		game,
		user,
	}));

	const dispatch = useDispatch<AppDispatch>();

	if (isNil(me)) return <>Illegal state!</>;

	if (isNil(currentOffer)) return <>Illegel state!</>;

	const ordinals = MessageHandler.getUserOrdinals(me, players, seed);

	const meNarrator = ordinals[me.id] === narratorOrdinal;

	const onGuess = (cardId: DeckCardId) => (
		dispatch(GameActions.guess(cardId))
	);

	if (meNarrator) {
		return (
			<div>
				<Image
					src={`/cards/${deck[currentOffer.cardId]}`}
					style={{
						cursor: 'pointer',
						height: 200,
						width: 'auto',
					}}
				/>
				{Object.entries(guesses).map(([userId, guess]) => (
					<>
						<Image
							src={`/cards/${deck[guess]}`}
							style={{
								cursor: 'pointer',
								height: 200,
								width: 'auto',
							}}
						/>
						{userId}
					</>
				))}
			</div>
		);
	}

	return (
		<>
			{currentOffer.description}
			{myHand.map(card => (
				<Image
					src={`/cards/${deck[card]}`}
					preview={false}
					onClick={() => onGuess(card)}
					style={{
						cursor: 'pointer',
						height: 200,
						width: 'auto',
					}}
				/>
			))}
		</>
	);
};

export default GuessesStage;