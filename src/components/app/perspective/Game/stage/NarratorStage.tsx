import { Image } from 'antd';
import MessageHandler from 'components/app/perspective/Game/messaging/MessageHandler';
import {
	DeckCardId,
	GameActions,
} from 'components/app/perspective/Game/redux';
import {
	isNil,
	noop,
} from 'lodash-es';
import { FC } from 'react';
import {
	useDispatch,
	useSelector,
} from 'react-redux';
import {
	AppDispatch,
	RootState,
} from 'redux/store';
import deck from 'resources/deck.json';

interface NarratorStageProps {
	[key: string]: unknown;
}

const NarrationStage: FC<NarratorStageProps> = () => {
	const {
		game: { myHand, narratorOrdinal, players, seed },
		user: { me },
	} = useSelector(({ game, user }: RootState) => ({
		game,
		user,
	}));

	const dispatch = useDispatch<AppDispatch>();

	if (isNil(me)) return <>Illegal state!</>;

	const ordinals = MessageHandler.getUserOrdinals(me, players, seed);

	const meNarrator = ordinals[me.id] === narratorOrdinal;

	const onCardSelected = (cardId: DeckCardId) => {
		const desc = prompt('Describe the card');

		if (isNil(desc)) return;

		void dispatch(GameActions.describe(cardId, desc));
	};

	return (
		<>
			{meNarrator
				? 'Me am narrator'
				: `${Object.keys(ordinals)[narratorOrdinal]} im narrator`
			}
			{myHand.map(card => (
				<Image
					src={`/cards/${deck[card]}`}
					preview={false}
					onClick={meNarrator ? () => onCardSelected(card) : noop}
					style={{
						cursor: 'pointer',
						height: 200,
						width: 'auto',
					}}
				/>
			))}
		</>
	);
};

export default NarrationStage;