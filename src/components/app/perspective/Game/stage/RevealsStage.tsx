import { Image } from 'antd';
import MessageHandler from 'components/app/perspective/Game/messaging/MessageHandler';
import {
	DeckCardId,
	GameActions,
} from 'components/app/perspective/Game/redux';
import {
	isNil,
	noop,
} from 'lodash-es';
import { FC } from 'react';
import {
	useDispatch,
	useSelector,
} from 'react-redux';
import {
	AppDispatch,
	RootState,
} from 'redux/store';
import deck from 'resources/deck.json';
import { shuffle } from 'util/collections';

interface NarratorStageProps {
	[key: string]: unknown;
}

const RevealsStage: FC<NarratorStageProps> = () => {
	const {
		game: { narratorOrdinal, players, seed, currentOffer, guesses, roundOrdinal },
		user: { me },
	} = useSelector(({ game, user }: RootState) => ({
		game,
		user,
	}));

	const dispatch = useDispatch<AppDispatch>();

	if (isNil(me)) return <>Illegal state!</>;

	if (isNil(currentOffer)) return <>Illegel state!</>;

	const ordinals = MessageHandler.getUserOrdinals(me, players, seed);

	const meNarrator = ordinals[me.id] === narratorOrdinal;

	const onReveal = (userId: string) => (
		dispatch(GameActions.reveal(userId))
	);

	if (meNarrator) {
		return (
			<div>
				<Image
					src={`/cards/${deck[currentOffer.cardId]}`}
					style={{
						cursor: 'pointer',
						height: 200,
						width: 'auto',
					}}
				/>
				{Object.entries(guesses).map(([userId, guess]) => (
					<>
						<Image
							src={`/cards/${deck[guess]}`}
							style={{
								cursor: 'pointer',
								height: 200,
								width: 'auto',
							}}
						/>
						{userId}
					</>
				))}
			</div>
		);
	}

	const narratorId = Object.keys(players)[narratorOrdinal];

	const guessing: [string, DeckCardId][] = shuffle(
		[[narratorId, currentOffer.cardId], ...Object.entries(guesses)],
		roundOrdinal,
	);

	return (
		<>
			Select one:
			{guessing.map(([id, card]) => (
				<>
					{(id === me?.id) && 'This is mine'}
					<Image
						key={card}
						src={`/cards/${deck[card]}`}
						preview={id === me?.id}
						onClick={(id === me?.id) ? noop : () => onReveal(id)}
						style={{
							cursor: 'pointer',
							height: 200,
							width: 'auto',
						}}
					/>
				</>
			))}
		</>
	);
};

export default RevealsStage;