import { GameActions } from 'components/app/perspective/Game/redux';
import { noop } from 'lodash-es';
import { FC } from 'react';
import {
	useDispatch,
	useSelector,
} from 'react-redux';
import {
	AppDispatch,
	RootState,
} from 'redux/store';

const PreGameStage: FC = () => {
	const { me, meReady, players, error, starting } = useSelector(({ game }: RootState) => game);
	const dispatch: AppDispatch = useDispatch();

	const allReady = Object.values(players)
		.map(p => p.ready)
		.reduce((p, c) => p && c, true);

	const minPlayers = Object.values(players).length >= 2;

	const onReady = () => dispatch(GameActions.meReady());
	const onStart = () => dispatch(GameActions.startGame());

	return (
		<div>
			<b color={'red'}>{error?.toString()}</b><br />
			<b>Me:</b> {me?.id}
			<b>Players:</b>
			{Object.entries(players).map(([id, player]) => (
				<div>
					<b>Id:</b> {id}<br />
					<b>Alias:</b> {player.alias} <br />
					<b>Active:</b> {player.active ? 'yes' : 'no'}
				</div>
			))}
			<button
				onClick={onReady}
				disabled={meReady}
			>
				Ready
			</button>
			<button
				onClick={onStart}
				disabled={!(meReady && allReady && minPlayers && !starting)}
			>
				Start
			</button>
		</div>
	);
};

export default PreGameStage;