import { PayloadAction } from '@reduxjs/toolkit';
import {
	createDesc,
	createGuess,
	createReveal,
	createRevealStarted,
} from 'components/app/perspective/Game/messaging';
import MessageHandler from 'components/app/perspective/Game/messaging/MessageHandler';
import {
	DeckCardId,
	GameActions,
	GameStage,
	GameState,
	Offer,
} from 'components/app/perspective/Game/redux';
import { isNil } from 'lodash-es';
import { AppThunk } from 'redux/store';
import { NoOpAction } from 'util/redux';

const actions = {
	setNarratorOrdinal: (
		{ ...state }: GameState,
		{ payload }: PayloadAction<number>,
	): GameState => ({
		...state,
		narratorOrdinal: payload,
	}),
	setOffer: (
		{ ...state }: GameState,
		{ payload }: PayloadAction<Offer>,
	): GameState => ({
		...state,
		currentOffer: payload,
	}),
	addGuess: (
		{ guesses, ...state }: GameState,
		{ payload: [userId, guess] }: PayloadAction<[string, DeckCardId]>,
	): GameState => ({
		...state,
		guesses: {
			...guesses,
			[userId]: guess,
		},
	}),
	addReveal: (
		{ reveals, ...state }: GameState,
		{ payload: [userId, reveal] }: PayloadAction<[string, string]>,
	): GameState => ({
		...state,
		reveals: {
			...reveals,
			[userId]: reveal,
		},
	}),
};

const thunkActions = {
	describe: (cardId: DeckCardId, description: string): AppThunk => (
		(dispatch, getState) => {
			const { game: { players } } = getState();

			dispatch(GameActions.setOffer({ cardId, description }));

			Object.values(players).forEach(player => {
				player.peer.send(createDesc({ cardId, description }));
			});

			dispatch(GameActions.setStage(GameStage.Guesses));

			return NoOpAction;
		}
	),
	guess: (cardId: DeckCardId): AppThunk => (
		(dispatch, getState) => {
			const { game: { players }, user: { me } } = getState();

			if (isNil(me)) return NoOpAction;

			dispatch(GameActions.addGuess([me.id, cardId]));

			Object.values(players).forEach(player => {
				player.peer.send(createGuess(cardId));
			});

			return dispatch(GameActions.checkGuesses());
		}
	),
	checkGuesses: (): AppThunk => (
		(dispatch, getState) => {
			const { game: { players, guesses, seed, narratorOrdinal }, user: { me } } = getState();

			const ordinals = MessageHandler.getUserOrdinals(me, players, seed);

			if (isNil(me)) return NoOpAction;

			const meGuess = ordinals[me.id] === narratorOrdinal ? true : !!guesses[me.id];
			const allGuessed = meGuess && Object.entries(players)
				.filter(([, player]) => player.active)
				.filter(([uid]) => ordinals[uid] !== narratorOrdinal)
				.map(([id]) => id)
				.map(id => !!guesses[id])
				.reduce((t, c) => (t && c), true);


			if (allGuessed) {
				dispatch(GameActions.setStage(GameStage.Reveals));

				Object.values(players).forEach(player => {
					player.peer.send(createRevealStarted());
				});
			}

			return NoOpAction;
		}
	),
	reveal: (userId: string): AppThunk => (
		(dispatch, getState) => {
			const { game: { players }, user: { me } } = getState();

			if (isNil(me)) return NoOpAction;

			dispatch(GameActions.addReveal([me.id, userId]));

			Object.values(players).forEach(player => {
				player.peer.send(createReveal(userId));
			});

			return dispatch(GameActions.checkReveals());
		}
	),
	checkReveals: (): AppThunk => (
		(dispatch, getState) => {
			const { game: { players, reveals, seed, narratorOrdinal }, user: { me } } = getState();

			const ordinals = MessageHandler.getUserOrdinals(me, players, seed);

			if (isNil(me)) return NoOpAction;

			const meReveal = ordinals[me.id] === narratorOrdinal ? true : !!reveals[me.id];

			const allRevealed = meReveal && Object.entries(players)
				.filter(([, player]) => player.active)
				.filter(([uid]) => ordinals[uid] !== narratorOrdinal)
				.map(([id]) => id)
				.map(id => !!reveals[id])
				.reduce((t, c) => (t && c), true);

			console.log(allRevealed);

			if (allRevealed) {
				dispatch(GameActions.setStage(GameStage.Results));
			}

			return NoOpAction;
		}
	),
};


const PLAY_GAME_ACTIONS = { actions, thunkActions };

export default PLAY_GAME_ACTIONS;