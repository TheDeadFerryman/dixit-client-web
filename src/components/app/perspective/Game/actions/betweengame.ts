import { PayloadAction } from '@reduxjs/toolkit';
import {
	createNextRound,
	createReadyNext,
} from 'components/app/perspective/Game/messaging';
import MessageHandler from 'components/app/perspective/Game/messaging/MessageHandler';
import {
	DeckCardId,
	GameActions,
	GameStage,
	GameState,
} from 'components/app/perspective/Game/redux';
import {
	compact,
	isEmpty,
	isNil,
	round,
} from 'lodash-es';
import { AppThunk } from 'redux/store';
import deck from 'resources/deck.json';
import { take } from 'util/collections';
import { NoOpAction } from 'util/redux';

const actions = {
	addReadyNext: (
		{ readyNext, ...state }: GameState,
		{ payload: [userId, ready] }: PayloadAction<[string, boolean]>,
	): GameState => ({
		...state,
		readyNext: {
			...readyNext,
			[userId]: ready,
		},
	}),
	setNextRound: (
		{ roundOrdinal, ...state }: GameState,
	): GameState => ({
		...state,
		roundOrdinal: roundOrdinal + 1,
	}),
	clearLastRound: (
		{...state}: GameState,
	): GameState => ({
		...state,
		guesses: {},
		reveals: {},
		currentOffer: undefined,
		readyNext: {},
	})
};

const thunkActions = {
	checkReady: (): AppThunk => (
		(dispatch, getState) => {
			const { game: { readyNext, players }, user: { me } } = getState();

			if (isNil(me)) return NoOpAction;

			const allReady = readyNext[me.id] && Object.entries(players)
				.filter(([, player]) => player.active)
				.map(([id]) => readyNext[id])
				.reduce((t, v) => (t && v), true);

			if (allReady) {
				void dispatch(GameActions.nextRound());
			}

			return NoOpAction;
		}
	),
	readyNext: (): AppThunk => (
		(dispatch, getState) => {
			const { game: { players }, user: { me } } = getState();

			if (isNil(me)) return NoOpAction;

			dispatch(GameActions.addReadyNext([me.id, true]));

			Object.values(players).forEach(player => (
				player.peer.send(createReadyNext())
			));

			return dispatch(GameActions.checkReady());
		}
	),
	nextRound: (): AppThunk => (
		async (dispatch, getState) => {
			const { game: { players, roundOrdinal } } = getState();

			await dispatch(GameActions.refreshHand());
			await dispatch(GameActions.nextNarrator());

			dispatch(GameActions.clearLastRound());

			dispatch(GameActions.setNextRound());

			Object.values(players).forEach(player => (
				player.peer.send(createNextRound(roundOrdinal))
			));

			return dispatch(GameActions.setStage(GameStage.Narration));
		}
	),
	refreshHand: (): AppThunk => (
		(dispatch, getState) => {
			const { game: { narratorOrdinal, currentOffer, players, seed, guesses }, user: { me } } = getState();

			if (isNil(me)) return NoOpAction;

			const ordinals = MessageHandler.getUserOrdinals(me, players, seed);

			if (ordinals[me.id] === narratorOrdinal) {
				if (isNil(currentOffer)) return NoOpAction;

				return dispatch(GameActions.replaceCardInHand(currentOffer.cardId));
			}

			const myCard = guesses[me.id];

			if (isNil(myCard)) return NoOpAction;

			return dispatch(GameActions.replaceCardInHand(myCard));
		}
	),
	replaceCardInHand: (cardId: DeckCardId): AppThunk => (
		(dispatch, getState) => {
			const { game: { seed, myHand, taken, players }, user: { me } } = getState();

			if (isNil(me)) return NoOpAction;

			const cardKeys = Object.keys(deck) as DeckCardId[];

			const ordinals = MessageHandler.getUserOrdinals(me, players, seed);

			const shift = round(cardKeys.length / (Object.keys(players).length + 1)) * ordinals[me.id];

			const replacer = take({
				collection: cardKeys,
				count: 1,
				seed, startFrom: (taken + shift),
			});

			if (isEmpty(replacer)) dispatch(GameActions.setError('Out of cards!'));

			const newHand = compact(myHand.map(card => (
				card === cardId ? replacer[0] : card
			)));

			dispatch(GameActions.setTaken(taken + 1));

			return dispatch(GameActions.setMyHand(newHand));
		}
	),
	nextNarrator: (): AppThunk => (
		(dispatch, getState) => {
			const { game: { narratorOrdinal, players, seed }, user: { me } } = getState();

			if (isNil(me)) return NoOpAction;

			const ordinals = MessageHandler.getUserOrdinals(me, players, seed);

			let next = narratorOrdinal + 1;

			// eslint-disable-next-line no-constant-condition
			while (true) {
				if (next >= Object.values(ordinals).length) {
					next = 0;
				}

				if (next == narratorOrdinal) {
					return dispatch(GameActions.setError('Something went wrong!'));
				}

				if (next == ordinals[me.id]) {
					return dispatch(GameActions.setNarratorOrdinal(next));
				}

				if (Object.values(players)[next].active) {
					return dispatch(GameActions.setNarratorOrdinal(next));
				} else {
					next += 1;
				}
			}
		}
	),
};

const BETWEEN_GAME_ACTIONS = { actions, thunkActions };

export default BETWEEN_GAME_ACTIONS;