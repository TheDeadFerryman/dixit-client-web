import { PayloadAction } from '@reduxjs/toolkit';
import {
	createGreeter,
	createReady,
	createStarted,
} from 'components/app/perspective/Game/messaging';
import MessageHandler from 'components/app/perspective/Game/messaging/MessageHandler';
import { AnyMessage } from 'components/app/perspective/Game/messaging/types';
import {
	DeckCardId,
	GameActions,
	GameStage,
	GameState,
} from 'components/app/perspective/Game/redux';
import LobbyApi, {
	JoinLobbyResult,
	LobbyPeer,
} from 'components/app/perspective/LobbyList/api';
import { ResponseKind } from 'jsonrpc';
import {
	isError,
	isNil,
	uniqueId,
} from 'lodash-es';
import Peer from 'peerjs';
import { AppThunk } from 'redux/store';
import { NoOpAction } from 'util/redux';
import { Optional } from 'util/types';

const actions = {
	setMePeer: (
		{ ...state }: GameState,
		{ payload }: PayloadAction<Peer>,
	): GameState => ({
		...state,
		me: payload,
	}),

	addPlayerPeer: (
		{ players, ...state }: GameState,
		{ payload: [meta, connection] }: PayloadAction<[LobbyPeer, Peer.DataConnection]>,
	): GameState => ({
		...state,
		players: {
			...players,
			[meta.userId]: {
				peer: connection,
				alias: meta.alias,
				active: false,
				ready: false,
			},
		},
	}),

	togglePeerActive: (
		{ players, ...state }: GameState,
		{ payload: [id, active] }: PayloadAction<[string, boolean]>,
	): GameState => ({
		...state,
		players: {
			...players,
			[id]: { ...players[id], active },
		},
	}),

	togglePeerReady: (
		{ players, ...state }: GameState,
		{ payload: [id, ready] }: PayloadAction<[string, boolean]>,
	): GameState => ({
		...state,
		players: {
			...players,
			[id]: { ...players[id], ready },
		},
	}),

	setCurrentLobby: (
		{ ...state }: GameState,
		{ payload }: PayloadAction<string>,
	): GameState => ({
		...state,
		lobbyId: payload,
	}),

	setError: (
		{ ...state }: GameState,
		{ payload }: PayloadAction<Optional<string>>,
	): GameState => ({
		...state,
		error: payload,
	}),

	setLoading: (
		{ ...state }: GameState,
		{ payload }: PayloadAction<boolean>,
	): GameState => ({
		...state,
		loading: payload,
	}),
	setStage: (
		{ ...state }: GameState,
		{ payload }: PayloadAction<GameStage>,
	): GameState => ({
		...state,
		stage: payload,
	}),
	setMeReady: (
		{ ...state }: GameState,
		{ payload }: PayloadAction<boolean>,
	): GameState => ({
		...state,
		meReady: payload,
	}),
	setStarting: (
		{ ...state }: GameState,
		{ payload }: PayloadAction<boolean>,
	): GameState => ({
		...state,
		starting: payload,
	}),
	setSeed: (
		{ ...state }: GameState,
		{ payload }: PayloadAction<number>,
	): GameState => ({
		...state,
		seed: payload,
	}),
	setMyHand: (
		{ ...state }: GameState,
		{ payload }: PayloadAction<DeckCardId[]>,
	): GameState => ({
		...state,
		myHand: payload,
	}),
	setTaken: (
		{ ...state }: GameState,
		{ payload }: PayloadAction<number>,
	): GameState => ({
		...state,
		taken: payload,
	}),
};

const thunkActions = {
	joinLobbyInternal: (lobbyId: string, password: string): AppThunk => (
		async (dispatch, getState) => {
			const { user: { me }, game: { me: peer } } = getState();

			GameActions.setError(undefined);
			GameActions.setLoading(true);

			if (isNil(me)) return NoOpAction;
			if (isNil(peer)) return NoOpAction;
			try {
				const rsp = await LobbyApi.join({
					lobbyId, password,
					userId: me.id,
					connector: peer.id,
				}, uniqueId());

				if (rsp.kind === ResponseKind.Error) {
					return dispatch(GameActions.setError(rsp.error.message));
				} else {
					dispatch(GameActions.setCurrentLobby(lobbyId));
					return dispatch(GameActions.connectPeers(rsp.result));
				}
			} catch (e) {
				return dispatch(GameActions.setError(
					isError(e) ? e.message : 'Internal error',
				));
			}
		}
	),
	joinLobby: (lobbyId: string, password: string): AppThunk => (
		(dispatch, getState) => {
			const peer = new Peer();

			peer.on('open', () => {
				dispatch(GameActions.setMePeer(peer));
				void dispatch(GameActions.joinLobbyInternal(lobbyId, password));
			});

			peer.on('error', err => dispatch(GameActions.setError(err)));

			peer.on('connection', conn => {
				conn.on('data', (data: AnyMessage) => {
					void new MessageHandler(dispatch, getState).handle(data, conn);
				});
			});

			return NoOpAction;
		}
	),
	initGame: (lobbyId: string, password: string): AppThunk => (
		dispatch => {
			dispatch(GameActions.setStage(GameStage.PreGame));
			return dispatch(GameActions.joinLobby(lobbyId, password));
		}
	),
	connectPeers: (data: JoinLobbyResult): AppThunk => (
		async dispatch => {
			await Promise.all(data.map(peer => dispatch(GameActions.connectPeer(peer))));

			return NoOpAction;
		}
	),
	connectPeer: (data: LobbyPeer): AppThunk => (
		(dispatch, getState) => {
			const { game: { me }, user: { me: meUser } } = getState();

			if (isNil(me)) return NoOpAction;
			if (isNil(meUser)) return NoOpAction;

			const connection = me.connect(data.connector);

			dispatch(GameActions.addPlayerPeer([data, connection]));

			const handler = new MessageHandler(dispatch, getState);

			connection.on('open', () => {
				dispatch(GameActions.togglePeerActive([data.userId, true]));
				connection.send(createGreeter(meUser));
			});
			connection.on('close', () => handler.handleClosed(data.userId, connection));
			connection.on('error', err => dispatch(
				GameActions.setError(err),
			));
			connection.on('data', (data: AnyMessage) => {
				void handler.handle(data, connection);
			});
			return NoOpAction;
		}
	),
	meReady: (): AppThunk => (
		(dispatch, getState) => {
			const { game: { players } } = getState();

			dispatch(GameActions.setMeReady(true));

			Object.values(players).forEach(player => (
				player.peer.send(createReady())
			));

			return NoOpAction;
		}
	),
	startGame: (): AppThunk => (
		async (dispatch, getState) => {
			const { game: { players, lobbyId } } = getState();

			try {
				const rsp = await LobbyApi.start({ lobbyId }, uniqueId());

				if (rsp.kind === ResponseKind.Error) {
					return dispatch(GameActions.setError(rsp.error.message));
				} else {
					dispatch(GameActions.setSeed(rsp.result));
				}

				new MessageHandler(dispatch, getState).startGame();

				Object.values(players).forEach(player => (
					player.peer.send(createStarted(rsp.result))
				));
			} catch (e) {
				dispatch(GameActions.setError(isError(e) ? e.message : 'Internal error'));
			}

			return NoOpAction;
		}
	),
};

const PRE_GAME_ACTIONS = { actions, thunkActions };

export default PRE_GAME_ACTIONS;