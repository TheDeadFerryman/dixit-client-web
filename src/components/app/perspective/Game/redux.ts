import { createSlice } from '@reduxjs/toolkit';
import BETWEEN_GAME_ACTIONS from 'components/app/perspective/Game/actions/betweengame';
import PLAY_GAME_ACTIONS from 'components/app/perspective/Game/actions/game';
import PRE_GAME_ACTIONS from 'components/app/perspective/Game/actions/pregame';
import Peer from 'peerjs';
import deck from 'resources/deck.json';

export type DeckCardId = keyof (typeof deck);

export interface GamePlayer {
	peer: Peer.DataConnection;
	active: boolean;
	ready: boolean;
	alias: string;
}

export enum GameStage {
	OutGame,
	PreGame,
	Election,
	Narration,
	Guesses,
	Reveals,
	Results,
	Sync
}

export interface Offer {
	cardId: DeckCardId;
	description: string;
}

export interface GameState {
	loading: boolean;
	error?: string;

	starting: boolean;
	stage: GameStage;
	lobbyId: string;
	seed: number;

	me?: Peer;
	timeshift: number;
	meReady: boolean;
	myHand: DeckCardId[];

	currentOffer?: Offer;

	guesses: Record<string, DeckCardId>;
	reveals: Record<string, string>;
	readyNext: Record<string, boolean>;

	roundOrdinal: number;

	taken: number;

	players: Record<string, GamePlayer>;
	narratorOrdinal: number;

	scores: Record<string, number>;
	deck: string[];
}

const initialState: GameState = {
	loading: false,

	stage: GameStage.OutGame,
	seed: 0,
	lobbyId: '',

	starting: false,

	timeshift: 0,
	meReady: false,
	players: {},
	narratorOrdinal: 0,

	guesses: {},
	reveals: {},
	readyNext: {},

	roundOrdinal: 0,

	myHand: [],
	taken: 0,

	deck: [],
	scores: {},
};

const { reducer, actions } = createSlice({
	name: 'GameActions',
	initialState,
	reducers: {
		...PRE_GAME_ACTIONS.actions,
		...PLAY_GAME_ACTIONS.actions,
		...BETWEEN_GAME_ACTIONS.actions,
	},
});

const thunkActions = {
	...PRE_GAME_ACTIONS.thunkActions,
	...PLAY_GAME_ACTIONS.thunkActions,
	...BETWEEN_GAME_ACTIONS.thunkActions,
};

const GameReducer = reducer;

const GameActions = {
	...actions,
	...thunkActions,
};

export {
	GameReducer,
	GameActions,
};