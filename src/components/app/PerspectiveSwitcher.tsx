import { FC } from 'react';
import { useSelector } from 'react-redux';
import { RootState } from 'redux/store';
import { AppPerspective } from 'components/app/redux';
import LobbyListPerspective from 'components/app/perspective/LobbyList/LobbyListPerspective';
import UserPerspective from 'components/app/perspective/User/UserPerspective';
import GamePerspective from 'components/app/perspective/Game/GamePerspective';

type PerspectiveSwitcherProps = Record<string, unknown>;

const PerspectiveSwitcher: FC<PerspectiveSwitcherProps> = () => {
	const { perspective } = useSelector(({ app }: RootState) => app);

	switch (perspective) {
	case AppPerspective.User:
		return <UserPerspective/>;
	case AppPerspective.LobbyList:
		return <LobbyListPerspective/>;
	case AppPerspective.Game:
		return <GamePerspective/>;
	default:
		return null;
	}
};

export default PerspectiveSwitcher;