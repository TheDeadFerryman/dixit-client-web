import { createSlice } from '@reduxjs/toolkit';
import APP_ACTIONS from 'components/app/redux/actions';

export enum AppPerspective {
	User,
	LobbyList,
	Game,
}

export interface AppState {
	perspective: AppPerspective;
}

const initialState: AppState = {
	perspective: AppPerspective.User,
};

const { reducer, actions } = createSlice({
	name: 'AppActions',
	initialState,
	reducers: {
		...APP_ACTIONS.actions,
	},
});

const thunkActions = {
	...APP_ACTIONS.thunkActions,
};

export const AppReducer = reducer;

export const AppActions = {
	...actions,
	...thunkActions,
};