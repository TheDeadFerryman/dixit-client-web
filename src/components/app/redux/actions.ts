import { PayloadAction } from '@reduxjs/toolkit';
import { GameActions } from 'components/app/perspective/Game/redux';
import { LobbyCredentials } from 'components/app/perspective/LobbyList/api';

import {
	AppActions,
	AppPerspective,
	AppState,
} from 'components/app/redux/index';
import {
	AppDispatch,
	AppThunk,
} from 'redux/store';
import { NoOpAction } from 'util/redux';

const actions = {
	selectPerspectiveInternal: (
		state: AppState,
		{ payload }: PayloadAction<AppPerspective>,
	): AppState => ({
		...state,
		perspective: payload,
	}),
};

const thunkActions = {
	selectPerspective: (perspective: AppPerspective): AppThunk => (
		(dispatch: AppDispatch) => {
			switch (perspective) {
			case AppPerspective.User:
			case AppPerspective.Game:
			case AppPerspective.LobbyList:
			}

			return dispatch(
				AppActions.selectPerspectiveInternal(perspective),
			);
		}
	),
	initGame: ({ lobbyId, password }: LobbyCredentials): AppThunk => (
		async dispatch => {
			await dispatch(GameActions.initGame(lobbyId, password));
			await dispatch(AppActions.selectPerspective(AppPerspective.Game));

			return NoOpAction;
		}
	),
};

const APP_ACTIONS = { actions, thunkActions };

export default APP_ACTIONS;