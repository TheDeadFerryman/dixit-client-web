import { Space } from 'antd';
import React, { ComponentType, FC } from 'react';

interface IconTextProps {
	icon: ComponentType;
	text: string;
}

const IconText: FC<IconTextProps> = ({ icon, text }) => (
	<Space>
		{React.createElement(icon)}
		{text}
	</Space>
);

export default IconText;